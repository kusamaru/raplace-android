package com.kusamaru.raplace

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.navigation.*
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.kusamaru.raplace.data.preference.EncryptedPreferenceManager
import com.kusamaru.raplace.ui.theme.RaplaceTheme
import com.kusamaru.raplace.ui.LoginScreen
import com.kusamaru.raplace.ui.main.MainScreen

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalAnimationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = EncryptedPreferenceManager.getEncryptedPreferences(applicationContext)
        // ログイン済ならfeedに直で飛ぶ
        val startDest = if (prefs.getString("username", null) != null) {
            "feed"
        } else {
            "login"
        }

        setContent {
            val navController = rememberAnimatedNavController()
            RaplaceTheme {
                this.window.statusBarColor = MaterialTheme.colors.primarySurface.hashCode()

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    MainNavHost(
                        navController = navController,
                        startDestination = startDest,
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun MainNavHost(
    navController: NavHostController,
    startDestination: String
) {
    AnimatedNavHost(navController, startDestination) {
        composable(
            route = "login",
            enterTransition = { scaleIn(initialScale = 0.9F) },
        ) {
            LoginScreen(onSuccess = { navController.navigate("feed") })
        }

        composable(
            route = "feed",
            enterTransition = { scaleIn(initialScale = 0.9F) },
            exitTransition = { scaleOut() },
            popEnterTransition = { slideInHorizontally() }
        ) {
            MainScreen(
                onLogout = { navController.navigate("login") },
            )
        }
    }
}