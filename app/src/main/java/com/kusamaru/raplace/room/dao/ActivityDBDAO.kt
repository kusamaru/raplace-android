package com.kusamaru.raplace.room.dao

import androidx.room.*
import com.kusamaru.raplace.room.entity.ActivityEntity
import io.reactivex.Completable

@Dao
interface ActivityDao {

    @Query("SELECT * FROM activity")
    fun getAll(): List<ActivityEntity>

    @Query("SELECT * FROM activity ORDER BY _publishedTimestamp LIMIT :pageSize OFFSET :pageIndex")
    fun getWithPaging(pageSize: Int, pageIndex: Int): List<ActivityEntity>

    @Query("SELECT * FROM activity WHERE _publishedTimestamp BETWEEN :from AND :to")
    fun getByTime(from: Long, to: Long): List<ActivityEntity>

    @Query("SELECT * FROM activity WHERE id IN (:id)")
    fun findActivity(id: String): ActivityEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg activities: ActivityEntity): Completable

    @Delete
    fun delete(activity: ActivityEntity): Completable
}