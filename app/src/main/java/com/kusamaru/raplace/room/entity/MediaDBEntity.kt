package com.kusamaru.raplace.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

enum class MediaKind(val value: String) {
    NORMAL("normal"), // normal media
    ICON("icon"),     // user icon
}

@Entity(primaryKeys = ["id"], tableName = "media")
data class MediaEntity (
    val id: String,
    @ColumnInfo(name = "media_kind") val mediaKind: MediaKind,
    @ColumnInfo(name = "attributed_to") val attributedTo: String?,
    @ColumnInfo(name = "media_type") val mediaType: String,
    @ColumnInfo(name = "data") val url: String,
)
