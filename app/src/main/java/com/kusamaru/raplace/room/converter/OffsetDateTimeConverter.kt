package com.kusamaru.raplace.room.converter

import androidx.room.TypeConverter
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

class OffsetDateTimeConverter {
    @TypeConverter
    fun fromTimestamp(value: String?): OffsetDateTime? {
        return value?.let {
            OffsetDateTime.parse(it)
        }
    }

    @TypeConverter
    fun odtToTimestamp(odt: OffsetDateTime?): String? {
        return odt?.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
    }

}