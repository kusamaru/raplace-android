package com.kusamaru.raplace.room.manager

import android.content.Context
import androidx.room.Room
import com.kusamaru.raplace.room.database.ActivityDatabase

object ActivityDBManager {
    @Volatile
    private lateinit var activityDatabase: ActivityDatabase

    fun getInstance(context: Context): ActivityDatabase {
        if (!::activityDatabase.isInitialized) {
            activityDatabase = Room.databaseBuilder(context, ActivityDatabase::class.java, "ActivityDB")
                .fallbackToDestructiveMigration()
                .build()
        }
        return activityDatabase
    }
}