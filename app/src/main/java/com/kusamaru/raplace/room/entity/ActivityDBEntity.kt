package com.kusamaru.raplace.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.OffsetDateTime

@Entity(tableName = "activity")
data class ActivityEntity (
    @PrimaryKey(autoGenerate = true) val id: Int,
    /** Activityのtypeをここに */
    val type: String,
    /** ActivityのjsonをtoStringする */
    val json: String,
    /** Jsonにpublishedがある場合は設定する */
    val _publishedTimestamp: Long?,
)