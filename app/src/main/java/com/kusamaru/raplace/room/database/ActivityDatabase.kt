package com.kusamaru.raplace.room.database

import androidx.room.*
import com.kusamaru.raplace.room.converter.ListConverter
import com.kusamaru.raplace.room.converter.OffsetDateTimeConverter
import com.kusamaru.raplace.room.dao.ActivityDao
import com.kusamaru.raplace.room.entity.ActivityEntity

@Database(entities = [ActivityEntity::class], version = 1, exportSchema = false)
@TypeConverters(OffsetDateTimeConverter::class, ListConverter::class)
abstract class ActivityDatabase: RoomDatabase() {
    abstract fun activityDao(): ActivityDao
}