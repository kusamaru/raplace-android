package com.kusamaru.raplace.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kusamaru.raplace.room.dao.MediaDao
import com.kusamaru.raplace.room.entity.MediaEntity

@Database(entities = [MediaEntity::class], version = 1, exportSchema = false)
abstract class MediaDatabase: RoomDatabase() {
    abstract fun mediaDao(): MediaDao
}