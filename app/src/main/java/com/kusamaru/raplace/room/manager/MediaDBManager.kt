package com.kusamaru.raplace.room.manager

import android.content.Context
import androidx.room.Room
import com.kusamaru.raplace.room.database.MediaDatabase

object MediaDBManager {
    @Volatile
    private lateinit var mediaDatabase: MediaDatabase

    fun getInstance(context: Context): MediaDatabase {
        if (!::mediaDatabase.isInitialized) {
            mediaDatabase = Room.databaseBuilder(context, MediaDatabase::class.java, "MediaDB")
                .fallbackToDestructiveMigration()
                .build()
        }
        return mediaDatabase
    }
}