package com.kusamaru.raplace.room.dao

import androidx.room.*
import com.kusamaru.raplace.room.entity.MediaEntity
import io.reactivex.Completable

@Dao
interface MediaDao {
    @Query("SELECT * FROM media")
    fun getAll(): List<MediaEntity>

    @Query("SELECT * FROM media WHERE attributed_to IN (:activityId)")
    fun findAttachmentsById(activityId: String): List<MediaEntity>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg entities: MediaEntity): Completable

    @Delete
    fun delete(media: MediaEntity): Completable
}