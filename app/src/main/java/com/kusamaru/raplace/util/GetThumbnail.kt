package com.kusamaru.raplace.util

import android.content.Context
import android.graphics.ImageDecoder
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import com.kusamaru.raplace.data.dataclass.MediaFileDataClass
import kotlin.time.Duration

fun getMediaFileData(context: Context, mimeType: String, uri: Uri): MediaFileDataClass? {
    return runCatching {
        when {
            mimeType.startsWith("image") -> getMediaFileDataFromImage(context, uri, mimeType)
            mimeType.startsWith("video") -> getMediaFileDataFromVideo(context, uri, mimeType)
            else -> null
        }
    }.getOrNull()
}

private fun getMediaFileDataFromImage(context: Context, uri: Uri, mimeType: String): MediaFileDataClass? {
    val contentResolver = context.contentResolver
    return runCatching {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val source = ImageDecoder.createSource(contentResolver, uri)
            MediaFileDataClass(
                uri,
                ImageDecoder.decodeBitmap(source),
                mimeType,
                getFilenameFromUri(context, uri),
            )
        } else {
            MediaFileDataClass(
                uri,
                MediaStore.Images.Media.getBitmap(contentResolver, uri),
                mimeType,
                getFilenameFromUri(context, uri),
            )
        }
    }.getOrNull()
}

private fun getMediaFileDataFromVideo(context: Context, uri: Uri, mimeType: String): MediaFileDataClass? {
    return runCatching {
        val mediaMetadataRetriever = MediaMetadataRetriever()
        mediaMetadataRetriever.setDataSource(context, uri)

        val duration = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)!!
        MediaFileDataClass(
            uri,
            mediaMetadataRetriever.frameAtTime!!,
            mimeType,
            getFilenameFromUri(context, uri),
            Duration.parse(duration),
        )
    }.getOrNull()
}