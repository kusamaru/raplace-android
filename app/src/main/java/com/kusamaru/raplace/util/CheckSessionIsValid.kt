package com.kusamaru.raplace.util

import android.content.SharedPreferences
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

/**
 * セッションが有効か検証する。
 * @return 有効ならtrue
 */
fun checkSessionIsValid(prefs: SharedPreferences): Boolean {
    val session = prefs.getString("sessionId", "")
    val expires = prefs.getString("expiresAt", "")

    if (session == null || expires == null) { return false }

    try {
        val fmt = DateTimeFormatter.RFC_1123_DATE_TIME
        val parsedExpiresAt = LocalDateTime.parse(expires, fmt)
        if (parsedExpiresAt < LocalDateTime.now()) {
            // session is expired
            return false
        }
    } catch (e: DateTimeParseException) {
        println("failed to parse `expiresAt`: $expires")
        return false
    }

    return true
}