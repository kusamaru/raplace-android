package com.kusamaru.raplace.util

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns

fun getFilenameFromUri(context: Context, uri: Uri?): String? {
    return uri?.let { uri ->
        val fileName: String?
        val cursor = context.contentResolver.query(uri, null, null, null, null)
        cursor?.moveToFirst()
        fileName = cursor?.getColumnIndex(OpenableColumns.DISPLAY_NAME)?.let { cursor.getString(it) }
        cursor?.close()
        return fileName
    }
}

fun getByteArrayFromContentUri(context: Context, uri: Uri): ByteArray? {
    return runCatching {
        val inputStream = context.contentResolver.openInputStream(uri)
        return inputStream?.readBytes()
    }.getOrNull()
}