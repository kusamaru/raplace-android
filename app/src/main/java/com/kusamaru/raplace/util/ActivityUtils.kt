package com.kusamaru.raplace.util

import androidx.core.net.toUri

/**
 * `http://example.com/user/hogehoge123`みたいなidを`@hogehoge123@example.com`みたいな形式にパースする
  */
fun String.toActivityName(): String? {
    return kotlin.runCatching {
        val uri = this.toUri()
        "@${uri.path?.split("/")?.last()}@${uri.host}"
    }.getOrNull()
}

/**
 * 先頭と末尾の<p>タグを消す
  */
fun String.removePTagsFromContent(): String {
    return this.removePrefix("<p>").removeSuffix("</p>")
}