package com.kusamaru.raplace.ui.main.home

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.get
import com.github.michaelbull.result.getError
import com.kusamaru.raplace.data.activitypub.Activity
import com.kusamaru.raplace.data.activitypub.ActivityPubRepository
import com.kusamaru.raplace.data.activitypub.PersonObject
import com.kusamaru.raplace.data.feed.FeedRepository
import com.kusamaru.raplace.data.login.SessionUseCase
import com.kusamaru.raplace.data.preference.EncryptedPreferenceManager
import com.kusamaru.raplace.room.manager.ActivityDBManager
import com.kusamaru.raplace.util.checkSessionIsValid
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.sync.Mutex
import kotlinx.datetime.*

class HomeViewModel(
    application: Application,
): AndroidViewModel(application) {
    companion object {
        const val LimitOfFeedEntries = 500
    }

    /**
     * ロックみたいな何かだけどMutexのような安全性はないのでScreenの画面更新用
     */
    private suspend fun <T> MutableStateFlow<Boolean>.with(function: suspend () -> T): T = withContext(Dispatchers.Default) {
        this@with.emit(true)
        val result = function()
        this@with.emit(false)

        return@withContext result
    }

    // Feed周り扱うリポジトリ
    private val feedRepository: FeedRepository

    // Activityの情報
    private val _feedData: MutableStateFlow<MutableList<Activity>> = MutableStateFlow(mutableListOf())
    val feedData: StateFlow<List<Activity>> = _feedData

    // Activity.attributedToからユーザー情報を引っ張るための奴
    private val _personIdToPersonInfo: MutableStateFlow<MutableMap<String, PersonObject>> =
        MutableStateFlow(LinkedHashMap(LimitOfFeedEntries, 0.75f, true))
    val personIdToPersonInfo: StateFlow<Map<String, PersonObject>> = _personIdToPersonInfo

    // 更新中かを確認する
    private var _isRefreshingFeed: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val isRefreshingFeed: StateFlow<Boolean> = _isRefreshingFeed

    // 最後にAPIにアクセスした時刻
    private val _lastApiAccessTime: MutableStateFlow<LocalDateTime?> = MutableStateFlow(null)

    init {
        // repositoryを入手
        feedRepository = FeedRepository(activityDao = ActivityDBManager.getInstance(application).activityDao())

        updateFeed()
    }

    /**
     * 単発のActivityを拾ってくる
     */
    fun getActivity(id: String) {
        // すでに更新中なら何もしない
        if (_isRefreshingFeed.value) {
            return
        }

        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            toast("An error occurred: $throwable", Toast.LENGTH_LONG)
        }
        viewModelScope.launch(exceptionHandler + Dispatchers.Default) {
            _isRefreshingFeed.with {
                val result = feedRepository.getActivity(id)
                when (result) {
                    is Ok -> {
                        // 反映
                        processOkFeedData(Ok(listOf(result.value)))
                    }
                    is Err -> toast("Failed to get activity\n${result.error}", Toast.LENGTH_LONG)
                }
                // 処理完了が早すぎるとアニメーションが完了しないで消えるっぽい？？
                // delay(100)
            }
        }
    }

    fun updateFeed() {
        // すでに更新中なら何もしない
        if (_isRefreshingFeed.value) {
            return
        }

        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            toast("An error occurred: $throwable", Toast.LENGTH_LONG)
        }
        viewModelScope.launch(exceptionHandler + Dispatchers.Default) {
            _isRefreshingFeed.with {
                val context = getApplication<Application>().applicationContext
                val prefs = EncryptedPreferenceManager.getEncryptedPreferences(context)
                // check and update session
                if (!checkSessionIsValid(prefs)) {
                    val updateSessionResult = SessionUseCase.updateSession(prefs)

                    if (updateSessionResult.getError() != null) {
                        return@with
                    }
                }

                val host = prefs.getString("host", "") ?: ""
                val sessionId = prefs.getString("sessionId", "") ?: ""

                // 最終アクセス時刻より先のデータだけもらってくる
                val from = _lastApiAccessTime.value?.toInstant(TimeZone.currentSystemDefault())?.epochSeconds
                val feedResult = withContext(Dispatchers.IO) {
                    feedRepository.getFeed(
                        host,
                        sessionId,
                        from,
                        to = null,
                    )
                }

                when (feedResult) {
                    is Ok -> {
                        // 無ならトーストでお知らせしてreturn
                        if (feedResult.value.isEmpty()) {
                            toast("Find nothing...", Toast.LENGTH_SHORT)
                            return@with
                        }

                        // 成功したのでアクセス時刻を登録
                        _lastApiAccessTime.value = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault())
                        // 反映
                        processOkFeedData(feedResult)
                        // wipe処理
                        wipeOldData()
                    }
                    is Err -> toast("Failed to reload feed screen\n${feedResult.error}", Toast.LENGTH_LONG)
                }
            }
        }
    }

    fun onClickLoadOldNotes() {
        // すでに更新中なら何もしない
        if (_isRefreshingFeed.value) {
            return
        }

        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            toast("An error occurred: $throwable", Toast.LENGTH_LONG)
        }
        viewModelScope.launch(exceptionHandler + Dispatchers.IO) {
            _isRefreshingFeed.with {
                val context = getApplication<Application>().applicationContext
                val prefs = EncryptedPreferenceManager.getEncryptedPreferences(context)
                // セッションを検証
                if (!checkSessionIsValid(prefs)) {
                    val updateSessionResult = SessionUseCase.updateSession(prefs)

                    if (updateSessionResult.getError() != null) {
                        return@with
                    }
                }

                val host = prefs.getString("host", "") ?: ""
                val sessionId = prefs.getString("sessionId", "") ?: ""

                // 一番古いエントリより前のデータだけ貰う
                // 一旦Announceとかその他は無視すんべ
                val to = _feedData.value
                    .filterIsInstance<Activity.NOTE>()
                    .minByOrNull { it.published }
                    ?.let { it.published.toInstant()?.epochSecond }
                val feedResult = feedRepository.getFeed(
                    host,
                    sessionId,
                    from = null,
                    to = to
                )

                when (feedResult) {
                    is Ok -> {
                        // 無ならトーストでお知らせしてreturn
                        if (feedResult.value.isEmpty()) {
                            toast("Find nothing...", Toast.LENGTH_SHORT)
                            return@with
                        }

                        // 反映
                        processOkFeedData(feedResult)
                        // wipe処理
                        wipeOldData()
                    }
                    is Err -> toast("Failed to reload feed screen\n${feedResult.error}", Toast.LENGTH_LONG)
                }

                println("num: ${feedData.value.size}")
            }
        }
    }

    /**
     * いい感じにResultを処理するよ
     */
    private fun processOkFeedData(feedResult: Ok<List<Activity>>) {
        viewModelScope.launch(Dispatchers.Default) {
            feedResult.value.forEach {
                // distinct
                if (!_feedData.value.contains(it)) {
                    _feedData.value.add(it)
                }
            }

            _feedData.update {
                // publishedで降順ソート
                it.sortByDescending {
                    when (it) {
                        is Activity.NOTE -> { it.published }
                        else -> { throw IllegalStateException("Activity other than Activity.NOTE is not allowed") }
                    }
                }
                // update actordata
                it.forEach {
                    when (it) {
                        is Activity.NOTE -> {
                            // personIdToPersonInfoに含まない場合
                            if (!_personIdToPersonInfo.value.containsKey(it.attributedTo)) {
                                ActivityPubRepository.getActorData(it.attributedTo)
                                    .also { it1 ->
                                        println(it1)
                                    }.get()
                                    ?.let { it1 ->
                                        _personIdToPersonInfo.value.put(it.attributedTo, it1)
                                    }
                            }
                        }
                        else -> {}
                    }
                }
                it
            }
        }
    }

    /**
     * 多すぎるデータを消す。sortしてから呼び出すこと
     */
    private fun wipeOldData() {
        viewModelScope.launch(Dispatchers.IO) {
            if (_feedData.value.size > LimitOfFeedEntries) { // 上限を超えた分は削除する
                _feedData.value = feedData.value.subList(0, LimitOfFeedEntries - 1).toMutableList()
            }

            // LinkedHashMapだから勝手に消してくれるらしいよなんか
        }
    }

    // トースト炊く
    private fun toast(msg: String, length: Int) = viewModelScope.launch(Dispatchers.Main) {
        Toast.makeText(getApplication<Application>().applicationContext, msg, length).show()
    }
}