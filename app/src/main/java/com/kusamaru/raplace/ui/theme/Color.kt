package com.kusamaru.raplace.ui.theme

import androidx.compose.ui.graphics.Color


val Red200 = Color(0xFFFFB58B)
val Red500 = Color(0xFFDB4A23)
val Red700 = Color(0xFFBC371F)
val Yellow500 = Color(0xFFE6D82C)
val Yellow700 = Color(0xFFE2AF1D)

val LightDefaultIconsColor = Color(0xFF464646)
val DarkDefaultIconsColor = Color(0xFFA6A6A6)
val ActivatedAnnounceColor = Color(0xFF05F0BD)