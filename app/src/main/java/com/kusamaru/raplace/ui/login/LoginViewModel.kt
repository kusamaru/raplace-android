package com.kusamaru.raplace.ui.login

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.github.michaelbull.result.get
import com.github.michaelbull.result.getError
import com.kusamaru.raplace.data.login.SessionUseCase
import com.kusamaru.raplace.data.preference.EncryptedPreferenceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class LoginViewModel(application: Application): AndroidViewModel(application) {
    private val _host = MutableStateFlow("")
    val host: StateFlow<String> = _host

    private val _username = MutableStateFlow("")
    val username: StateFlow<String> = _username

    private val _password = MutableStateFlow("")
    val password: StateFlow<String> = _password

    // initialize flows
    init {
        val context = getApplication<Application>().applicationContext
        val prefs = EncryptedPreferenceManager.getEncryptedPreferences(context)
        _host.value = prefs.getString("host", "") ?: ""
        _username.value = prefs.getString("username", "") ?: ""
        _password.value = prefs.getString("password", "") ?: ""
    }

    fun onChangeHost(value: String) {
        viewModelScope.launch {
            _host.value = value
        }
    }

    fun onChangeUsername(value: String) {
        viewModelScope.launch {
            _username.value = value
        }
    }

    fun onChangePassword(value: String) {
        viewModelScope.launch {
            _password.value = value
        }
    }

    /**
     * ログインをする
     * @param onSuccess 成功時に実行する処理。navigateとか
     */
    fun tryLogin(onSuccess: () -> Unit) {
        val context = getApplication<Application>().applicationContext
        viewModelScope.launch(Dispatchers.Main) {
            val prefs = EncryptedPreferenceManager.getEncryptedPreferences(context)
            val login = SessionUseCase.tryLogin(prefs, _host.value, _username.value, _password.value)
            if (login.get() != null) {
                // success
                Toast.makeText(context, "Login Success", Toast.LENGTH_SHORT).show()
                onSuccess()
            } else {
                // fail
                Toast.makeText(context, "Login Failed: ${login.getError()}", Toast.LENGTH_LONG).show()
            }
        }
    }
}