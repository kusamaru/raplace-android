package com.kusamaru.raplace.ui.main.media

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import coil.compose.AsyncImage
import com.kusamaru.raplace.composable.TopAppBarWithReturn

@Composable
fun MediaScreen(
    onReturn: () -> Unit,
    mediaUrl: String
) {
    Scaffold(
        topBar = {
            TopAppBarWithReturn(title = "", onClickReturn = onReturn)
        }
    ) {
        Box(Modifier.padding(it)) {
            AsyncImage(
                modifier = Modifier.fillMaxWidth(),
                model = mediaUrl,
                contentDescription = mediaUrl
            )
        }
    }
}