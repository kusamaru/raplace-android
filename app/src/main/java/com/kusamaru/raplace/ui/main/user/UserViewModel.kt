package com.kusamaru.raplace.ui.main.user

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.getError
import com.kusamaru.raplace.data.activitypub.ActivityPubRepository
import com.kusamaru.raplace.data.login.SessionUseCase
import com.kusamaru.raplace.data.preference.EncryptedPreferenceManager
import com.kusamaru.raplace.util.checkSessionIsValid
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class UserViewModel(application: Application): AndroidViewModel(application) {
    private val _followTargetUserId: MutableStateFlow<String> = MutableStateFlow("")
    val followTargetUserId: StateFlow<String> = _followTargetUserId

    private val _iconUrl: MutableStateFlow<String> = MutableStateFlow("")
    val iconUrl: StateFlow<String> = _iconUrl

    fun setFollowTargetUserId(value: String) {
        viewModelScope.launch {
            _followTargetUserId.value = value
        }
    }

    fun setIconUrl(value: String) {
        viewModelScope.launch {
            _iconUrl.value = value
        }
    }

    fun sendFollowRequest() {
        viewModelScope.launch(Dispatchers.IO) {
            val context = getApplication<Application>().applicationContext
            val prefs = EncryptedPreferenceManager.getEncryptedPreferences(context)
            // check and update session
            if (!checkSessionIsValid(prefs)) {
                val updateSessionResult = SessionUseCase.updateSession(prefs)

                if (updateSessionResult.getError() != null) {
                    return@launch
                }
            }

            val host = prefs.getString("host", "") ?: ""
            val username = prefs.getString("username", "") ?: ""
            val sessionId = prefs.getString("sessionId", "") ?: ""

            val result = ActivityPubRepository.tryFollow(
                host,
                username,
                sessionId,
                _followTargetUserId.value
            )

            // エラーならログ
            if (result.getError() != null) {
                println(result)
            }

            withContext(Dispatchers.Main) {
                when (result) {
                    is Ok ->  Toast.makeText(context, "Follow request is successfully sent", Toast.LENGTH_LONG).show()
                    is Err -> Toast.makeText(context, "Error: $result", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun tryUpdateUserData() {
        viewModelScope.launch(Dispatchers.IO) {

        }
    }
}