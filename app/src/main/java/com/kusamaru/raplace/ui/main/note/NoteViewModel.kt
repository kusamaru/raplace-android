package com.kusamaru.raplace.ui.main.note

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.get
import com.kusamaru.raplace.data.activitypub.Activity
import com.kusamaru.raplace.data.activitypub.ActivityPubRepository
import com.kusamaru.raplace.data.activitypub.PersonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class NoteViewModel: ViewModel() {
    private val _activity: MutableStateFlow<Activity?> = MutableStateFlow(null)
    val activity: StateFlow<Activity?> = _activity

    private val _actorData: MutableStateFlow<PersonObject?> = MutableStateFlow(null)
    val actorData: StateFlow<PersonObject?> = _actorData

    private val _isLoading: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val isLoading: StateFlow<Boolean> = _isLoading

    fun getActivityById(id: String) {
        if (isLoading.value) {
            return
        }

        viewModelScope.launch(Dispatchers.IO) {
            _isLoading.value = true

            _activity.value = when (val a = ActivityPubRepository.getActivityById<Activity.NOTE>(id)) {
                is Ok -> a.value
                is Err -> {
                    println(a.error)
                    null
                }
            }
            _activity.value?.let {
                if (it is Activity.NOTE) {
                    getActorData(it.attributedTo)
                }
            }

            _isLoading.value = false
        }
    }

    private fun getActorData(url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _actorData.value = ActivityPubRepository.getActorData(url).get()
        }
    }
}