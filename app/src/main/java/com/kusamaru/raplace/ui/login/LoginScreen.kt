package com.kusamaru.raplace.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kusamaru.raplace.ui.login.LoginViewModel

/**
 * なんかいい感じのログイン画面。
 */
@Composable
fun LoginScreen(
    viewModel: LoginViewModel = viewModel(),
    onSuccess: () -> Unit
) {
    Box(
        Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        contentAlignment = Alignment.Center
    ) {
        val host by viewModel.host.collectAsState()
        val username by viewModel.username.collectAsState()
        val password by viewModel.password.collectAsState()

        InputCredentialsComponent(
            host,
            username,
            password,
            onChangeHost = viewModel::onChangeHost,
            onChangeName = viewModel::onChangeUsername,
            onChangePass = viewModel::onChangePassword,
            onClickLogin = viewModel::tryLogin,
            onSuccess = onSuccess
        )
    }
}

/**
 * 資格情報の入力画面。
 */
@Preview(showBackground = true)
@Composable
fun InputCredentialsComponent(
    host: String = "example.com",
    username: String = "",
    password: String = "",
    onChangeHost: (String) -> Unit = {},
    onChangeName: (String) -> Unit = {},
    onChangePass: (String) -> Unit = {},
    onClickLogin: (() -> Unit) -> Unit = {},
    onSuccess: () -> Unit = {},
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        OutlinedTextField(
            value = host,
            label = { Text(text = "Host(e.g. example.com)") },
            onValueChange = onChangeHost,
            modifier = Modifier.padding(10.dp),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Ascii,
                imeAction = ImeAction.Next
            )
        )
        OutlinedTextField(
            value = username,
            label = { Text(text = "Username") },
            onValueChange = onChangeName,
            modifier = Modifier.padding(10.dp),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Ascii,
                imeAction = ImeAction.Next
            )
        )
        OutlinedTextField(
            value = password,
            label = { Text(text = "Password") },
            onValueChange = onChangePass,
            modifier = Modifier.padding(10.dp),
            visualTransformation = PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            )
        )
        Button(
            modifier = Modifier.padding(8.dp),
            onClick = { onClickLogin(onSuccess) }
        ) {
            Text(text = "Login")
        }
    }
}