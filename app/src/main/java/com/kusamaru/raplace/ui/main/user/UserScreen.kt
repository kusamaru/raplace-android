package com.kusamaru.raplace.ui.main.user

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel

@Composable
fun UserScreen(
    viewModel: UserViewModel = viewModel(),
    onClickLogout: () -> Unit
) {
    val followUserId by viewModel.followTargetUserId.collectAsState()
    val iconUrl by viewModel.iconUrl.collectAsState()

    Column(
        modifier = Modifier
            .padding(16.dp)
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Row(
            // modifier = Modifier.padding(16.dp)
            verticalAlignment = Alignment.CenterVertically
        ) {
            OutlinedTextField(
                label = { Text(text = "Follow by ID") },
                value = followUserId,
                onValueChange = viewModel::setFollowTargetUserId
            )

            Button(onClick = viewModel::sendFollowRequest) {
                Text(text = "Try Follow")
            }
        }

//        Spacer(modifier = Modifier.height(32.dp))
//
//        Row(
//            // modifier = Modifier.padding(16.dp)
//            verticalAlignment = Alignment.CenterVertically
//        ) {
//            OutlinedTextField(
//                label = { Text(text = "Icon Url") },
//                value = iconUrl,
//                onValueChange = viewModel::setIconUrl
//            )
//
//            Button(onClick = viewModel::tryUpdateUserData) {
//                Text(text = "Update")
//            }
//        }

        Spacer(modifier = Modifier.height(32.dp))

        Button(onClick = onClickLogout) {
            Text(text = "Logout")
        }
    }
}