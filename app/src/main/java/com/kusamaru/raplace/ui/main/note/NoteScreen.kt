package com.kusamaru.raplace.ui.main.note

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kusamaru.raplace.composable.Note
import com.kusamaru.raplace.data.activitypub.Activity
import com.kusamaru.raplace.util.toActivityName
import org.jsoup.Jsoup
import java.time.format.DateTimeFormatter

@Composable
fun NoteScreen(
    activityId: String,
    viewModel: NoteViewModel = viewModel(),
    onClickCreateReply: (String) -> Unit,
    onClickAttachment: (String) -> Unit,
    onAnnounce: (String) -> Unit,
    onReturn: () -> Unit,
) {
    val activity by viewModel.activity.collectAsState()
    val actorData by viewModel.actorData.collectAsState()
    val isLoading by viewModel.isLoading.collectAsState()

    // Activityを取得する
    if (activity == null) {
        viewModel.getActivityById(activityId)
    }

    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TopAppBar(
            title = {},
            navigationIcon = {
                IconButton(onClick = onReturn) {
                    Icon(Icons.Filled.ArrowBack, contentDescription = "Back to previous screen")
                }
            },
        )

        if (isLoading) {
            // なんかいい感じの読み込みアニメーション付けたい
            Spacer(modifier = Modifier)
        } else {
            when (val a = activity) {
                is Activity.NOTE -> {
                    val dateTimeFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")

                    Note(
                        name = a.attributedTo.toActivityName() ?: "",
                        content = a.content,
                        published = a.published.format(dateTimeFormat),
                        inReplyTo = a.inReplyTo,
                        actorIconLink = actorData?.icon?.url,
                        attachment = a.attachment,
                        onClick = { /* 何もしない */ },
                        onAnnounce = { onAnnounce(a.id) },
                        onCreateReply = { onClickCreateReply(a.id) },
                        onClickImage = {  }
                    )
                }

                else -> {
                    Text(text = "Failed to get Note Object.")
                }
            }
        }
    }
}