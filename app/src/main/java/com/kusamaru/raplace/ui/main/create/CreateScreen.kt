package com.kusamaru.raplace.ui.main.create

import android.content.Intent
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.kusamaru.raplace.data.dataclass.MediaFileDataClass

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun CreateScreen(
    viewModel: CreateViewModel = viewModel(),
    inReplyTo: String?,
    onReturn: () -> Unit,
    onSuccess: (String) -> Unit,
) {
    // inReplyToを設定
    viewModel.setInReplyTo(inReplyTo)

    val content by viewModel.content.collectAsState()
    val replyTargetActivity by viewModel.replyTargetActivity.collectAsState()
    val attachment by viewModel.attachment.collectAsState()
    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        viewModel.addMedia(it)
    }
    val configuration = LocalConfiguration.current
    val padding by animateDpAsState(
        targetValue = if (WindowInsets.isImeVisible) {
            ButtonDefaults.MinHeight
        } else {
            0.dp
        }
    )
    val focusRequester = remember { FocusRequester() }

    CreateCompose(
        content = content,
        inReplyTo = inReplyTo,
        attachment = attachment,
        focusRequester = focusRequester,
        onChangeContent = viewModel::setContent,
        onRemoveAttachment = viewModel::removeMedia,
        onConfirm = { viewModel.createNote(onSuccess) },
        onReturn = onReturn,
        onSelectMedia = {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                type = "*/*"
                putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*", "video/*"))
                addCategory(Intent.CATEGORY_OPENABLE)
            }
            launcher.launch(intent)
        },
    )
}

@Preview(showBackground = true)
@Composable
fun CreateCompose(
    content: String = "",
    inReplyTo: String? = null,
    attachment: List<MediaFileDataClass> = listOf(),
    focusRequester: FocusRequester = FocusRequester(),
    onChangeContent: (String) -> Unit = {},
    onRemoveAttachment: (Uri) -> Unit = {},
    onConfirm: () -> Unit = {},
    onReturn: () -> Unit = {},
    onSelectMedia: () -> Unit = {}
) {
    Scaffold(
        topBar = { TopBar(onReturn, onConfirm) },
        bottomBar = { BottomInfomationBar(
            charsCount = content.length,
            linesCount = content.count { it == '\n' } + 1
        ) },
        floatingActionButton = {
            ExtendedFloatingActionButton(
                onClick = onSelectMedia,
                text = { Text(text = "Attach media") },
                icon = { Icon(Icons.Filled.Add, contentDescription = "add icon") },
                modifier = Modifier.padding(bottom = 4.dp),
            )
        }
    ) {
        Column(Modifier.padding(it)) {
            // inReplyTo field
            inReplyTo?.let {
                Text(text = "Reply to: $it", fontSize = 14.sp)
            }

            // attachment field
            LazyRow(Modifier.padding(4.dp)) {
                attachment.forEach {
                    item {
                        Box {
                            Image(
                                painter = rememberAsyncImagePainter(it.thumbnail),
                                contentDescription = "Attached Media",
                                modifier = Modifier.size(96.dp)
                            )
                            // remove button
                            IconButton(
                                onClick = { onRemoveAttachment(it.uri) },
                                modifier = Modifier
                                    .background(color = Color.White, shape = CircleShape)
                                    .align(Alignment.TopEnd)
                                    .padding(4.dp)
                                    .size(12.dp)
                            ) {
                                Icon(
                                    Icons.Filled.Close,
                                    contentDescription = "Remove this Media",
                                    tint = Color.Black
                                )
                            }
                        }
                    }
                }
            }

            // content
            Row(
                Modifier
                    .focusRequester(focusRequester)
                    .padding(4.dp)
                    .weight(1f)
            ) {
                OutlinedTextField(
                    value = content,
                    onValueChange = onChangeContent,
                    placeholder = { Text(text = "Content", style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Medium)) },
                    textStyle = TextStyle(fontSize = 17.sp, fontWeight = FontWeight.Medium),
                    modifier = Modifier,
                    colors = TextFieldDefaults.outlinedTextFieldColors(
                        focusedBorderColor = Color.Transparent,
                        unfocusedBorderColor = Color.Transparent
                    )
                )
            }
            // フォーカス発火
            LaunchedEffect(Unit) {
                focusRequester.requestFocus()
            }
        }
    }
}

@Composable
fun TopBar(
    onReturn: () -> Unit = {},
    onConfirm: () -> Unit = {},
) {
    TopAppBar(
        title = { Text(text = "Create") },
        navigationIcon = {
            IconButton(onClick = onReturn) {
                Icon(Icons.Filled.ArrowBack, contentDescription = "Back to previous screen")
            }
        },
        actions = {
            // post
            OutlinedButton(
                modifier = Modifier.clip(RoundedCornerShape(10.dp)),
                onClick = onConfirm
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(text = "Post Note")
                    Icon(
                        Icons.Filled.Send,
                        contentDescription = "Post the Note",
                        modifier = Modifier.padding(start = 8.dp)
                    )
                }
            }
        }
    )
}

@Composable
fun BottomInfomationBar(
    charsCount: Int,
    linesCount: Int,
) {
    BottomAppBar(
        Modifier.height(24.dp)
    ) {
        // Metadata field
        Row(
            Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Text(text = "chars: $charsCount", fontSize = 16.sp, fontWeight = FontWeight.Medium)
            Text(text = "lines: $linesCount", fontSize = 16.sp, fontWeight = FontWeight.Medium)
        }
    }
}