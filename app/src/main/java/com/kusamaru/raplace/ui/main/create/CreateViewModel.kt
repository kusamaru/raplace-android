package com.kusamaru.raplace.ui.main.create

import android.app.Application
import android.net.Uri
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.kusamaru.raplace.data.activitypub.Activity
import com.kusamaru.raplace.data.activitypub.ActivityPubRepository
import com.kusamaru.raplace.data.activitypub.MediaObject
import com.kusamaru.raplace.data.dataclass.MediaByteArrayDataClass
import com.kusamaru.raplace.data.dataclass.MediaFileDataClass
import com.kusamaru.raplace.data.preference.EncryptedPreferenceManager
import com.kusamaru.raplace.data.raplace.RaplaceAPIRepository
import com.kusamaru.raplace.util.checkSessionIsValid
import com.kusamaru.raplace.util.getByteArrayFromContentUri
import com.kusamaru.raplace.util.getMediaFileData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.OffsetDateTime

class CreateViewModel(application: Application): AndroidViewModel(application) {
    private val _content: MutableStateFlow<String> = MutableStateFlow("")
    val content: StateFlow<String> = _content

    private val _inReplyTo: MutableStateFlow<String?> = MutableStateFlow(null)

    private val _replyTargetActivity: MutableStateFlow<Activity?> = MutableStateFlow(null)
    val replyTargetActivity: StateFlow<Activity?> = _replyTargetActivity

    private val _attachment: MutableStateFlow<MutableList<MediaFileDataClass>> = MutableStateFlow(mutableListOf())
    val attachment: StateFlow<List<MediaFileDataClass>> = _attachment


    fun setContent(value: String) {
        viewModelScope.launch {
            _content.value = value
        }
    }

    fun setInReplyTo(value: String?) {
        viewModelScope.launch {
            _inReplyTo.value = value
        }
    }

    fun loadReplyTarget() {
        viewModelScope.launch(Dispatchers.IO) {
        }
    }

    fun addMedia(result: ActivityResult) {
        if (result.resultCode != android.app.Activity.RESULT_OK) { return }

        val context = getApplication<Application>().applicationContext
        when {
            // select multiple files
            (result.data?.clipData != null) -> {
                result.data?.clipData?.let {
                    for (i in 0 until it.itemCount) {
                        val uri = it.getItemAt(i).uri
                        val type = it.description.getMimeType(i)

                        getMediaFileData(context, type, uri)?.let {
                            _attachment.value.add(it)
                        }
                    }
                }
            }

            // select only one file
            (result.data?.data != null) -> {
                result.data?.data?.let { uri ->
                    val type = result.data?.resolveType(context) ?: ""

                    getMediaFileData(context, type, uri)?.let {
                        _attachment.value.add(it)
                    }
                }
            }

            else -> {}
        }
    }

    fun removeMedia(uri: Uri) {
        viewModelScope.launch {
            // URIで削除
            _attachment.value = _attachment.value.filter { it.uri != uri } as MutableList<MediaFileDataClass>
        }
    }

    /**
     * Noteを作ってSharedPreferences.hostのoutboxに投げる。
     */
    fun createNote(
        onSuccess: (String) -> Unit,
    ) {
        viewModelScope.launch {
            val context = getApplication<Application>().applicationContext
            val prefs = EncryptedPreferenceManager.getEncryptedPreferences(context)

            // セッションが無効なら消える
            if (!checkSessionIsValid(prefs)) {
                withContext(Dispatchers.Main) {
                    Toast.makeText(context, "Session is invalid.", Toast.LENGTH_LONG).show()
                }
                return@launch
            }

            val host = prefs.getString("host", "") ?: ""
            val username = prefs.getString("username", "") ?: ""
            val sessionId = prefs.getString("sessionId", "") ?: ""

            // アタッチメントがあればあらかじめ投げてIDを拾う
            var attachmentList: List<MediaObject>? = null
            if (_attachment.value.isNotEmpty()) {
                // 必要なデータに加工
                val attachmentByteArrayList = attachment.value.mapIndexed { index, it ->
                    getByteArrayFromContentUri(context, it.uri)?.let { ba ->
                        val fileSuffix = it.fileName?.substringAfterLast(".", missingDelimiterValue = "")
                        MediaByteArrayDataClass(
                            byteArray = ba,
                            it.mimeType,
                            "$index.$fileSuffix" // ファイル名に2バイト文字が含まれてるとエラー帰ってくる
                        )
                    }
                }.filterNotNull()

                when (val result = RaplaceAPIRepository.uploadMedia(attachmentByteArrayList, host, sessionId)) {
                    is Ok -> { attachmentList = result.value }
                    is Err -> {
                        withContext(Dispatchers.Main) {
                            Toast.makeText(context, result.error, Toast.LENGTH_LONG).show()
                        }
                        return@launch
                    }
                }
            }

            // 送信用の仮Noteを作成する
            // 空文字列になってる部分はサーバー側で補完される
            val note = Activity.NOTE(
                id = "",
                content = _content.value,
                attributedTo = "",
                inReplyTo = _inReplyTo.value,
                published = OffsetDateTime.now(),
                attachment = attachmentList,
            )

            println(note.toString())

            val sendResult = ActivityPubRepository.sendActivityToOutbox(
                host,
                username,
                note,
                sessionId,
            )

            when (sendResult) {
                is Ok -> {
                    onSuccess(sendResult.value)
                }
                is Err -> {
                    withContext(Dispatchers.Main) {
                        Toast.makeText(context, sendResult.error, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }
}