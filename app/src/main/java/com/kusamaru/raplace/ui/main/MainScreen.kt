package com.kusamaru.raplace.ui.main

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.Notifications
import androidx.compose.material.icons.outlined.Person
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.navigation.*
import androidx.navigation.compose.currentBackStackEntryAsState
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.kusamaru.raplace.R
import com.kusamaru.raplace.composable.MainSideDrawer
import com.kusamaru.raplace.ui.main.create.CreateScreen
import com.kusamaru.raplace.ui.main.home.HomeScreen
import com.kusamaru.raplace.ui.main.media.MediaScreen
import com.kusamaru.raplace.ui.main.note.NoteScreen
import com.kusamaru.raplace.ui.main.notifications.NotificationScreen
import com.kusamaru.raplace.ui.main.user.UserScreen
import kotlinx.coroutines.coroutineScope

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun MainScreen(
    navController: NavHostController = rememberAnimatedNavController(),
    onLogout: () -> Unit = {}
) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    Scaffold(
        drawerContent = { MainSideDrawer(onLogout) },
        drawerGesturesEnabled = false,
        topBar = { FeedAppBar(
            currentDestination = currentDestination,
            onClickButton = {}
        )},
        bottomBar = { BottomNavigationBar(
            navController = navController,
            currentDestination = currentDestination
        )},
    ) {
        Box(Modifier.padding(it)) {
            // メインの画面
            AnimatedNavHost(
                navController = navController,
                startDestination = "home",
            ) {
                // ホーム画面。フィードが表示される
                composable(
                    FeedNavigationItem.HOME.route,
                ) {
                    HomeScreen(
                        navController = navController,
                        onClickNote = { navController.navigate("viewNote?id=$it") },
                        onClickCreate = { inReplyTo ->
                            if (inReplyTo != null) {
                                navController.navigate("createNote?inReplyTo=$inReplyTo")
                            } else {
                                navController.navigate("createNote")
                            }
                        },
                        onClickImage = { url ->
                            navController.navigate("media?url=$url")
                        }
                    )
                }

                // 通知
                composable(
                    FeedNavigationItem.NOTIFICATIONS.route
                ) {
                    NotificationScreen()
                }

                // ユーザー情報？
                composable(
                    FeedNavigationItem.USER.route
                ) {
                    UserScreen(onClickLogout = onLogout)
                }

                // サブ画面(ナビゲーションバーにない部分)
                navigation(startDestination = "viewNote", route = "subScreens") {
                    // note作成画面
                    composable(
                        route = "createNote?inReplyTo={inReplyTo}",
                        arguments = listOf(
                            navArgument("inReplyTo") {
                                type = NavType.StringType
                                nullable = true
                            }
                        )
                    ) {
                        val inReplyTo = it.arguments?.getString("inReplyTo")

                        CreateScreen(
                            inReplyTo = inReplyTo,
                            onReturn = { navController.popBackStack() },
                            onSuccess = {
                                navController.previousBackStackEntry?.run {
                                    savedStateHandle["resultNoteId"] = it
                                }
                                navController.popBackStack()
                            },
                        )
                    }

                    // 全画面でNote見る
                    composable(
                        route = "viewNote?id={id}",
                        arguments = listOf(navArgument("id") { type = NavType.StringType }),
                    ) { backStackEntry ->
                        val id = backStackEntry.arguments?.getString("id") ?: ""
                        NoteScreen(
                            activityId = id,
                            onClickAttachment = { url ->
                                navController.navigate("media?url=$url")
                            },
                            onClickCreateReply = { inReplyTo ->
                                navController.navigate("createNote?inReplyTo=$inReplyTo")
                            },
                            onAnnounce = {  },
                            onReturn = { navController.popBackStack() },
                        )
                    }

                    // 全画面でMediaみる
                    composable(
                        route = "media?url={url}",
                        arguments = listOf(navArgument("url") { type = NavType.StringType }),
                    ) { backStackEntry ->
                        val url = backStackEntry.arguments?.getString("url") ?: ""
                        MediaScreen(onReturn = { navController.popBackStack() }, mediaUrl = url)
                    }
                }
            }
        }
    }
}

@Composable
fun FeedAppBar(
    modifier: Modifier = Modifier,
    currentDestination: NavDestination?,
    onClickButton: () -> Unit,
) {
    // メインなら表示するよ
    val isCurrentDestinationIsMain = FeedNavigationItem.values().any { currentDestination?.route == it.route }
    if (isCurrentDestinationIsMain) {
        TopAppBar(
            title = {
                Icon(painter = painterResource(id = R.mipmap.ic_launcher_foreground), contentDescription = "App Icon")
            },
            navigationIcon = {
                IconButton(onClick = onClickButton) {
                    Icon(Icons.Filled.Menu, contentDescription = "Show Menu")
                }
            }
        )
    }
}

@Composable
fun BottomNavigationBar(
    modifier: Modifier = Modifier,
    currentDestination: NavDestination?,
    navController: NavController,
) {
    // メインなら表示するよ
    val isCurrentDestinationIsMain = FeedNavigationItem.values().any { currentDestination?.route == it.route }
    if (isCurrentDestinationIsMain) {
        BottomNavigation(modifier) {
            val backStack by navController.currentBackStackEntryAsState()
            val current = backStack?.destination?.route

            FeedNavigationItem.values().forEach { item ->
                BottomNavigationItem(
                    selected = current == item.route,
                    icon = { Icon(item.icon, null) },
                    onClick = {
                        navController.navigate(item.route) {
                            popUpTo(id = 0) {
                                saveState = true
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    },
                )
            }
        }
    }
}

enum class FeedNavigationItem(
    val route: String,
    val icon: ImageVector,
    private val typeId: Int,
) {
    HOME("home", Icons.Outlined.Home, 0),
    NOTIFICATIONS("notifications", Icons.Outlined.Notifications, 1),
    USER("user", Icons.Outlined.Person, 2);

    companion object {
        fun valueOf(typeId: Int): FeedNavigationItem? {
            return FeedNavigationItem.values().filter { it.typeId == typeId }.firstOrNull()
        }
    }
}