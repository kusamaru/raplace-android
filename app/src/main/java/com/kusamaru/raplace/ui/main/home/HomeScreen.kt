package com.kusamaru.raplace.ui.main.home

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.kusamaru.raplace.composable.Note
import com.kusamaru.raplace.data.activitypub.Activity
import com.kusamaru.raplace.util.toActivityName
import kotlinx.coroutines.launch
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Document.OutputSettings
import org.jsoup.safety.Safelist
import java.time.format.DateTimeFormatter

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreen(
    viewModel: HomeViewModel = viewModel(),
    navController: NavController,
    onClickNote: (String) -> Unit,
    onClickCreate: (String?) -> Unit,
    onClickImage: (String) -> Unit,
) {
    val isRefreshing by viewModel.isRefreshingFeed.collectAsState()
    val feedData by viewModel.feedData.collectAsState()
    val personIdToPersonInfo by viewModel.personIdToPersonInfo.collectAsState()
    val dateTimeFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")

    // Pull-to-refresh
    val refreshScope = rememberCoroutineScope()

    fun refresh() = refreshScope.launch {
        viewModel.updateFeed()
    }
    val pullRefreshState = rememberPullRefreshState(
        refreshing = isRefreshing,
        onRefresh = ::refresh
    )

    // createNoteから戻ってきてるか確認　そうならIDのNoteを取得してListに加える
    navController.currentBackStackEntry?.run {
        val resultNoteId = savedStateHandle.get<String>("resultNoteId")
        if (resultNoteId != null) {
            viewModel.getActivity(resultNoteId)
            savedStateHandle.remove<String>("resultNoteId")
        }
    }

    // compose
    Box(
        Modifier
            .pullRefresh(pullRefreshState)
            .fillMaxSize()
    ) {
        Column {
            if (true) {
                LazyColumn() {
                    feedData.forEach {
                        when (it) {
                            is Activity.NOTE -> {
                                item {
                                    Note(
                                        name = it.attributedTo.toActivityName() ?: "",
                                        content = it.content,
                                        published = it.published.format(dateTimeFormat),
                                        inReplyTo = it.inReplyTo,
                                        actorIconLink = personIdToPersonInfo[it.attributedTo]?.icon?.url,
                                        attachment = it.attachment,
                                        onClick = { onClickNote(it.id) },
                                        onCreateReply = { onClickCreate(it.id) },
                                        onAnnounce = {},
                                        onClickImage = onClickImage,
                                    )
                                }
                            }
                            else -> {}
                        }
                    }

                    item {
                        Note(
                            name = "",
                            content = "Load more posts...",
                            published = "",
                            actorIconLink = "NONE://PLACEHOLDER",
                            attachment = null,
                            onClick = viewModel::onClickLoadOldNotes,
                            onCreateReply = null,
                            onAnnounce = null
                        )
                    }
                }
            }
        }

        FloatingActionButton(
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(16.dp),
            onClick = { onClickCreate(null) },
        ) {
            Icon(Icons.Filled.Add, contentDescription = "Create New Note")
        }

        if (pullRefreshState.progress > 0 || isRefreshing) PullRefreshIndicator(
            refreshing = isRefreshing,
            state = pullRefreshState,
            modifier = Modifier.align(Alignment.TopCenter),
            scale = true
        )
    }
}