package com.kusamaru.raplace.data.activitypub

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.*
import java.util.concurrent.TimeUnit
import com.github.michaelbull.result.Result
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.kusamaru.raplace.data.dataclass.MediaByteArrayDataClass
import com.kusamaru.raplace.data.feed.FeedRepository
import com.kusamaru.raplace.data.raplace.RaplaceAPIService
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

object ActivityPubRepository {
    const val TIMEOUT_MILLISEC = 10000.toLong()
    const val READ_TIMEOUT_MILLISEC = 100000.toLong()

    /**
     * actor情報を拾ってくる。
     * @param url ユーザーIDのURL
     */
    suspend fun getActorData(url: String): Result<PersonObject?, String> = withContext(Dispatchers.IO) {
        try {
            val client = OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_MILLISEC, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT_MILLISEC, TimeUnit.MILLISECONDS)
                .build()

            val request = Request.Builder()
                .url(url)
                .addHeader("Accept", "application/activity+json")
                .build()

            val response = client.newCall(request).execute()
            if (response.isSuccessful) {
                return@withContext Ok(response.body
                    ?.let {
                        val fmt = Json { ignoreUnknownKeys = true }
                        fmt.decodeFromString<PersonObject>(it.string())
                    })
            }

            return@withContext Err(response.toString())
        } catch (e: Exception) {
            return@withContext Err(e.toString())
        }
    }

    /**
     * Activityを拾ってくる
     */
    suspend inline fun <reified T: @Serializable Any> getActivityById(id: String): Result<T?, String> = withContext(Dispatchers.IO) {
        try {
            val client = OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_MILLISEC, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT_MILLISEC, TimeUnit.MILLISECONDS)
                .build()

            val request = Request.Builder()
                .url(id)
                .addHeader("Accept", "application/activity+json")
                .build()

            val response = client.newCall(request).execute()
            if (response.isSuccessful) {
                kotlin.runCatching {
                    response.body?.let {
                        val fmt = Json { ignoreUnknownKeys = true }
                        fmt.decodeFromString<T>(it.string())
                    }
                }
                    .onSuccess { return@withContext Ok(it) }
                    .onFailure { return@withContext Err(it.toString()) }
            }
            // Failure
            return@withContext Err(response.toString())
        } catch (e: Exception) {
            return@withContext Err(e.toString())
        }
    }

    private val json = Json {
        encodeDefaults = true
        classDiscriminator = "changedType"
    }

    /**
     * Followリクエストをサーバーに投げる
     */
    @OptIn(ExperimentalSerializationApi::class)
    suspend fun tryFollow(host: String, username: String, sessionId: String, followTargetUrl: String): Result<Unit, String> = withContext(Dispatchers.IO) {
        val response = kotlin.runCatching {
            var baseUrl = "https://$host"
            if (baseUrl.last() != '/') { baseUrl += '/' }
            val client = OkHttpClient.Builder().build()
            val apApiService = Retrofit.Builder()
                .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
                .baseUrl(baseUrl)
                .client(client)
                .build()
                .create(ActivityPubAPIService::class.java)

            apApiService.postActivity(
                cookie = "session_id=$sessionId",
                username = username,
                activity = Activity.FOLLOW(object_ = followTargetUrl)
            )
        }.getOrElse { return@withContext Err("Request Error: $it") }

        if (response.isSuccessful) {
            return@withContext Ok(Unit)
        } else {
            return@withContext Err("Response Error: ${response.code()}")
        }
    }

    /**
     * Activityをoutboxに投げる
     */
    @OptIn(ExperimentalSerializationApi::class)
    suspend fun sendActivityToOutbox(
        host: String,
        username: String,
        activity: Activity,
        sessionId: String
    ): Result<String, String> = withContext(Dispatchers.IO) {
        val response =
            runCatching {
                var baseUrl = "https://$host"
                if (baseUrl.last() != '/') { baseUrl += '/' }
                val client = OkHttpClient.Builder().build()
                val apApiService = Retrofit.Builder()
                    .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
                    .baseUrl(baseUrl)
                    .client(client)
                    .build()
                    .create(ActivityPubAPIService::class.java)

                apApiService.postActivity(
                    cookie = "session_id=$sessionId",
                    username = username,
                    activity = activity
                )
            }.getOrElse { return@withContext Err(it.toString()) }

        if (!response.isSuccessful || response.headers()["Location"] == null) {
            return@withContext Err("Post Error: $response")
        }

        return@withContext Ok(response.headers()["Location"] ?: "")
    }
}