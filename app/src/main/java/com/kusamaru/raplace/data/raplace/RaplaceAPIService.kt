package com.kusamaru.raplace.data.raplace

import com.kusamaru.raplace.data.login.UserCredentials
import kotlinx.serialization.json.JsonObject
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

/**
 * Raplace APIを取り扱うService
 */
interface RaplaceAPIService {
    @POST("api/login")
    suspend fun tryLogin(
        @Body credentials: UserCredentials
    ): Response<Void>

    @GET("api/getfeed")
    suspend fun getFeed(
        @Header("Cookie") cookie: String,
        @Query("page") page: Int? = null,
        @Query("from") from: Long? = null,
        @Query("to") to: Long? = null,
    ): Response<JsonObject>

    @JvmSuppressWildcards // 付けないとジェネリクスに翻訳されてIllegalArgumentExcepる
    @Multipart
    @POST("api/upload_media")
    suspend fun uploadMedia(
        @Header("Cookie") cookie: String,
        @PartMap params: Map<String, RequestBody>
    ): Response<JsonObject>
}

@kotlinx.serialization.Serializable
data class UserCredentials (
    val username: String,
    val password: String
)