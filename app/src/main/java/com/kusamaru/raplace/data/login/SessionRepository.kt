package com.kusamaru.raplace.data.login

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.kusamaru.raplace.data.raplace.RaplaceAPIService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.lang.Exception
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object SessionRepository {
    /**
     * https://{$host}/a/loginに対してログインを試みる。
     * @return 成功した場合は有効なセッション。失敗したら例外のcontext情報
     */
    suspend fun authenticate(host: String, username: String, password: String): Result<SessionDataClass?, String> = withContext(Dispatchers.Default) {
        try {
            var baseUrl = "https://$host"
            if (baseUrl.last() != '/') { baseUrl += '/' }
            val client = OkHttpClient.Builder().build()
            val loginService = Retrofit.Builder()
                .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
                .baseUrl(baseUrl)
                .client(client)
                .build()
                .create(RaplaceAPIService::class.java)

            val response = loginService.tryLogin(UserCredentials(username, password))
            if (response.isSuccessful) {
                val sessionCookie = response.headers()["Set-Cookie"]?.split(";")
                return@withContext Ok(SessionDataClass(
                    sessionId = sessionCookie?.get(0)!!
                            .replace("%22", "")
                            .replace("session_id=", ""),
                    expiresAt = sessionCookie
                            .filter { it.contains("Expires=") }[0]
                            .replace("Expires=", "")
                            .trim(),
                ))
            } else {
                return@withContext Err(response.message())
            }
        }
        catch (e: Exception) {
            when(e) {
                is UnknownHostException -> {}
                is SocketTimeoutException -> {}
                else -> throw e
            }
            println("Error: " + e.message)
            return@withContext Err(e.toString())
        }
    }
}