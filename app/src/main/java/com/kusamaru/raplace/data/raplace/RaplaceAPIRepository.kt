package com.kusamaru.raplace.data.raplace

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.kusamaru.raplace.data.activitypub.MediaObject
import com.kusamaru.raplace.data.dataclass.MediaByteArrayDataClass
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

object RaplaceAPIRepository {
    private val json = Json {
        encodeDefaults = true
        classDiscriminator = "changedType"
    }

    @OptIn(ExperimentalSerializationApi::class)
    suspend fun uploadMedia(
        attachments: List<MediaByteArrayDataClass>,
        host: String,
        sessionId: String,
    ): Result<List<MediaObject>?, String> = withContext(Dispatchers.IO) {
        val response =
            runCatching {
                var baseUrl = "https://$host"
                if (baseUrl.last() != '/') { baseUrl += '/' }
                val client = OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
                    .build()
                val raplaceApiService = Retrofit.Builder()
                    .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
                    .baseUrl(baseUrl)
                    .client(client)
                    .build()
                    .create(RaplaceAPIService::class.java)

                // 加工
                val params = mutableMapOf<String, RequestBody>()
                attachments.forEach {
                    val body = it.byteArray.toRequestBody()
                    params[it.fileName ?: ""] = body
                }

                raplaceApiService.uploadMedia(
                    cookie = "session_id=$sessionId",
                    params = params,
                )
            }.getOrElse { return@withContext Err(it.toString()) }

        if (response.isSuccessful) {
            runCatching {
                val fmt = Json { ignoreUnknownKeys = true }
                return@withContext Ok(response.body()
                    ?.get("result")
                    ?.jsonArray
                    ?.map {
                        val inner = it.jsonObject
                        when (inner["type"]?.jsonPrimitive?.contentOrNull) {
                            "Success" -> {
                                println("upload success: ${inner["name"]}")
                                MediaObject(
                                    mediaType = attachments.filter {
                                        it.fileName == inner["name"]?.jsonPrimitive?.contentOrNull
                                    }.firstOrNull()?.mimeType ?: throw IllegalArgumentException("mediaType is invalid"),
                                    url = inner["available_on"]?.jsonPrimitive?.contentOrNull ?: throw IllegalArgumentException("available_on is invalid")
                                )
                            }
                            "Failure" -> return@withContext Err("upload failed: ${inner["name"]}, ${inner["reason"]}")
                            else -> null
                        }
                    }?.filterNotNull())
            }.onFailure { return@withContext Err(it.toString()) }
        }
        return@withContext Err("todo")
    }
}