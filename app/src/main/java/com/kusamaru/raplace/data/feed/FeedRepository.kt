package com.kusamaru.raplace.data.feed

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.kusamaru.raplace.data.activitypub.Activity
import com.kusamaru.raplace.data.activitypub.toActivity
import com.kusamaru.raplace.data.activitypub.toActivityEntity
import com.kusamaru.raplace.data.raplace.RaplaceAPIService
import com.kusamaru.raplace.room.dao.ActivityDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.*
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import java.io.IOException
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


// そのうちDIしたいのでシングルトンやめる
class FeedRepository(private val activityDao: ActivityDao) {
    /**
     * List<Activity>を手に入れるべ~
     */
    @OptIn(ExperimentalSerializationApi::class)
    suspend fun getFeed(
        host: String,
        sessionId: String,
        from: Long?,
        to: Long?,
    ): Result<List<Activity>, String> = withContext(Dispatchers.IO) {
        // とりあえずRoomアクセスして持ってこられないか試す
        when (val result = getFeedFromRoom(from, to)) {
            is Ok -> {
                return@withContext result
            }
            is Err -> { /* 続行 */ }
        }

        // Webアクセスで引っ張る
        when (val result = getFeedFromWeb(host, sessionId, from, to)) {
            is Ok -> {
                // DBに挿入
                activityDao.insertAll(*result.value.toActivityEntity().toTypedArray())
                return@withContext result
            }
            is Err -> return@withContext Err("Error. ${result.error}")
        }
    }

    /**
     * Webから引っ張ってくる。
     */
    private suspend fun getFeedFromWeb(
        host: String,
        sessionId: String,
        from: Long?,
        to: Long?,
    ): Result<List<Activity>, String> = withContext(Dispatchers.IO) {
        // get feed from host
        val response =
            try {
                var baseUrl = "https://$host"
                if (baseUrl.last() != '/') { baseUrl += '/' }
                val client = OkHttpClient.Builder().build()
                val raplaceApiService = Retrofit.Builder()
                    .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
                    .baseUrl(baseUrl)
                    .client(client)
                    .build()
                    .create(RaplaceAPIService::class.java)

                raplaceApiService.getFeed(
                    cookie = "session_id=$sessionId",
                    page = 1,
                    from = from,
                    to = to,
                )
            } catch (e: java.lang.Exception) {
                return@withContext Err(e.toString())
            }

        if (response.isSuccessful) {
            try {
                val fmt = Json { ignoreUnknownKeys = true }
                val ret = response.body()
                    ?.get("orderedItems")
                    ?.jsonArray
                    ?.map {
                        // orderedItemsのentry
                        it.let { fmt.decodeFromJsonElement<Activity.NOTE>(it) }
                    } ?: listOf()

                return@withContext Ok(ret)
            } catch (e: SerializationException) {
                println("failed to parse feed json")
                return@withContext Err(e.toString())
            }
        } else {
            return@withContext Err("Unknown Error")
        }
    }

    /**
     * Roomから引っ張ってくる。Unitが帰ってきたら存在しないってことで
     */
    suspend fun getFeedFromRoom(
        from: Long?,
        to: Long?,
    ): Result<List<Activity>, Unit> = withContext(Dispatchers.IO) {
        val result = activityDao.getByTime(from ?: 0, to ?: Long.MAX_VALUE)
        if (result.isNotEmpty()) {
            return@withContext Ok(result.toActivity())
        } else {
            return@withContext Err(Unit)
        }
    }

    /**
     * Activityをidから取得する。
     * @return 有効なActivityでない場合はErr
     */
    suspend fun getActivity(
        id: String
    ): Result<Activity, String> = suspendCoroutine { continuation ->
        val client = OkHttpClient.Builder().build()
        val request = Request.Builder()
            .url(id)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful && response.body != null) {
                    val fmt = Json { ignoreUnknownKeys = true }
                    val jsonStr = response.body!!.string()
                    continuation.resume(Ok(fmt.decodeFromString(jsonStr)))
                } else {
                    continuation.resume(Err(response.message))
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                println("Error: $e")
                continuation.resume(Err(e.toString()))
            }
        })
    }
}