package com.kusamaru.raplace.data.activitypub

import retrofit2.Response
import retrofit2.http.*

interface ActivityPubAPIService {
    @POST("u/{username}/outbox")
    suspend fun postActivity(
        @Header("Content-Type") contentType: String = "application/activity+json",
        @Header("Cookie") cookie: String,
        @Path("username") username: String,
        @Body activity: Activity
    ): Response<Void>
}