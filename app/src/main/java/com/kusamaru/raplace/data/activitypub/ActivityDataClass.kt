package com.kusamaru.raplace.data.activitypub

import com.kusamaru.raplace.room.entity.MediaEntity
import com.kusamaru.raplace.room.entity.MediaKind
import com.kusamaru.raplace.room.entity.ActivityEntity
import com.kusamaru.raplace.util.OffsetDateTimeSerializer
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.time.OffsetDateTime
import kotlinx.serialization.json.*

const val NOTE_STR = "Note"
const val FOLLOW_STR = "Follow"
const val ANNNOUNCE_STR = "Announce"

/**
 * Activity(Object)のTypeを列挙してあるやつ
 */
private enum class AType(val str: String) {
    NOTE(str = "Note"),
    FOLLOW(str = "Follow"),
    ANNOUNCE(str = "Announce"),
}

@Serializable
sealed class Activity(
    @SerialName("@context")
    @Serializable(with = StringListSerializer::class)
    private val __context: String = "https://www.w3.org/ns/activitystreams",
    @SerialName("type")
    private val __type: String,
) {
    /**
     * ObjectのTypeを返す
     */
    fun getType(): String {
        return this.__type
    }

    /**
     * 本来はActivityじゃなくてObjectだけど簡単のためにActivityとする
     * 実装変更が必要になったら変える
     */
    @Serializable
    @SerialName(NOTE_STR)
    data class NOTE(
        val id: String,
        val attributedTo: String,
        val inReplyTo: String?,
        val content: String,
        @Serializable(with = OffsetDateTimeSerializer::class)
        val published: OffsetDateTime,
        val attachment: List<MediaObject>?,
    ) : Activity(__type = AType.NOTE.str)

    /**
     * Announce Activity
     */
    @Serializable
    @SerialName(ANNNOUNCE_STR)
    data class ANNOUNCE(
        val id: String,
        val actor: String,
        @Serializable(with = OffsetDateTimeSerializer::class)
        val published: OffsetDateTime?,
        @SerialName("object")
        val object_: String,
    ) : Activity(__type = AType.ANNOUNCE.str)

    @Serializable
    @SerialName(FOLLOW_STR)
    data class FOLLOW(
        @SerialName("object")
        val object_: String
    ) : Activity(__type = AType.FOLLOW.str)
}

object StringListSerializer :
    JsonTransformingSerializer<String>(serializer()) {
    // If response is not an array, then it is a single object
    override fun transformDeserialize(element: JsonElement): JsonElement =
        if (element !is JsonArray) element.jsonPrimitive else element[0]
}

@Serializable
data class MediaObject (
    @SerialName("type")
    private val __type: String = "Document",
    val mediaType: String,
    val url: String,
)

// 拡張関数

// パーサ
private val jsonParser = Json {
    ignoreUnknownKeys = true
    classDiscriminator = "changedType"
}

/**
 * ActivityEntityに変換する。attachmentsがあればList<MediaEntity>にして返す
 * */
fun Activity.toActivityEntity(): ActivityEntity {
    val published = when (this) {
        is Activity.NOTE -> this.published.toEpochSecond()
        is Activity.ANNOUNCE -> this.published?.toEpochSecond()
        is Activity.FOLLOW -> null
    }
    return ActivityEntity(
        id = 0, // 仮採番なので0を返す。Insert時にautoincrementで自動採番されるはず
        type = this.getType(),
        json = jsonParser.encodeToString(this),
        _publishedTimestamp = published
    )
}
fun List<Activity>.toActivityEntity(): List<ActivityEntity> {
    return map { it.toActivityEntity() }
}

/**
 * EntityからActivityへ
 */
fun ActivityEntity.toActivity(): Activity {
    return when (this.type) {
        AType.NOTE.str -> jsonParser.decodeFromString<Activity.NOTE>(this.json)
        AType.ANNOUNCE.str -> jsonParser.decodeFromString<Activity.ANNOUNCE>(this.json)
        AType.FOLLOW.str -> jsonParser.decodeFromString<Activity.FOLLOW>(this.json)
        else -> throw IllegalArgumentException("Unknown activity type")
    }
}
fun List<ActivityEntity>.toActivity(): List<Activity> {
    return map { it.toActivity() }
}