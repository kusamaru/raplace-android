package com.kusamaru.raplace.data.login


@kotlinx.serialization.Serializable
data class UserCredentials (
    val username: String,
    val password: String,
)

data class SessionDataClass (
    val sessionId: String,
    val expiresAt: String,
)
