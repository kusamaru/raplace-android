package com.kusamaru.raplace.data.dataclass

import android.graphics.Bitmap
import android.net.Uri
import kotlin.time.Duration


data class MediaFileDataClass(
    val uri: Uri,
    val thumbnail: Bitmap,
    val mimeType: String,
    val fileName: String?,
    val mediaLength: Duration? = null,
)

data class MediaByteArrayDataClass(
    val byteArray: ByteArray,
    val mimeType: String,
    val fileName: String?,
)