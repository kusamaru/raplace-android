package com.kusamaru.raplace.data.activitypub

import kotlinx.serialization.Serializable

@Serializable
data class PersonObject(
    val id: String,
    val name: String?,
    val preferredUsername: String?,
    val summary: String?,
    val url: String,
    val inbox: String,
    val outbox: String,
    val icon: MediaObject?,
)