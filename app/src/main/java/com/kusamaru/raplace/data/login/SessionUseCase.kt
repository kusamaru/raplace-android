package com.kusamaru.raplace.data.login

import android.content.Context
import android.content.SharedPreferences
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result

object SessionUseCase {
    /**
     * ログインする。
     * @return 成功したか否か。Errの場合はcontextをStringとして返す
     */
    suspend fun tryLogin(prefs: SharedPreferences, host: String, username: String, password: String): Result<Unit, String> {
        val loginResult = SessionRepository.authenticate(
            host = host,
            username = username,
            password = password
        )

        when (loginResult) {
            is Ok -> {
                with (prefs.edit()) {
                    putString("host", host)
                    putString("username", username)
                    putString("password", password)
                    putString("sessionId", loginResult.value?.sessionId)
                    putString("expiresAt", loginResult.value?.expiresAt)
                    apply()
                }

                return Ok(Unit)
            }
            is Err -> {
                return Err(loginResult.error)
            }
        }
    }

    /**
     * EncryptedSharedPreferenceの資格情報に基づいてセッションを更新する。
     * @return 失敗したら詳細がStringで帰る
     */
    suspend fun updateSession(prefs: SharedPreferences): Result<Unit, String> {
        val host = prefs.getString("host", "") ?: ""
        val username = prefs.getString("username", "") ?: ""
        val password = prefs.getString("password", "") ?: ""

        return tryLogin(prefs, host, username, password)
    }

    /**
     * ログアウトする
     */
    suspend fun dropSession(prefs: SharedPreferences) {
        with (prefs.edit()) {
            remove("sessionId")
            remove("expiresAt")
            apply()
        }
    }
}