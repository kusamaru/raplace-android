package com.kusamaru.raplace.composable

import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun MainSideDrawer(
    onLogout: () -> Unit,
) {
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    ModalDrawer(
        drawerState = drawerState,
        drawerContent = {  
            Text(text = "Raplace", modifier = Modifier.padding(16.dp))
            Divider()
        },
    ) {

        Button(onClick = onLogout) {
            Text(text = "Logout")
        }
    }
}