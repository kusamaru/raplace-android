package com.kusamaru.raplace.composable

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.kusamaru.raplace.data.activitypub.MediaObject
import com.kusamaru.raplace.ui.theme.ActivatedAnnounceColor
import com.kusamaru.raplace.util.forwardingPainter
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.safety.Safelist

val outputSettings = Document.OutputSettings().prettyPrint(false)

@Composable
fun Note(
    name: String,
    content: String,
    published: String,
    inReplyTo: String? = null,
    actorIconLink: String?,
    attachment: List<MediaObject>? = null,
    onClick: () -> Unit,
    onCreateReply: (() -> Unit)?,
    onAnnounce: (() -> Unit)?,
    onClickImage: (String) -> Unit = {},
) {
    Column {
        val cleanedContent = Jsoup.clean(content, "", Safelist.none(), outputSettings)
        NoteContents(name, cleanedContent, published, inReplyTo, actorIconLink, onClick)

        if (attachment != null && attachment.isNotEmpty()) {
            NoteAttachments(
                attachment = attachment,
                onClickImage = onClickImage
            )
        }

        if (onCreateReply != null || onAnnounce != null) {
            NoteActions(
                onCreateReply,
                onAnnounce,
            )
        }

        Divider()
    }
}

@Composable
fun NoteContents(
    name: String,
    content: String,
    published: String,
    inReplyTo: String?,
    actorIconLink: String?,
    onClick: () -> Unit,
) {
    Box(
        Modifier.clickable(onClick = onClick)
    ) {
        Row(
            Modifier.padding(8.dp)
        ) {
            // TODO: 仮修正 もっと良い方法あるはず
            if (actorIconLink != "NONE://PLACEHOLDER") {
                // icon
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(actorIconLink)
                        .crossfade(true)
                        .build(),
                    contentDescription = "Icon of user",
                    error = forwardingPainter(
                        painter = rememberVectorPainter(image = Icons.Filled.Person),
                        colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
                    ),
                    modifier = Modifier
                        .size(56.dp)
                        .padding(end = 8.dp),
                )
            } else {
                Spacer(modifier = Modifier
                    .size(56.dp)
                    .padding(end = 8.dp))
            }

            // content
            Column {
                // username
                Text(
                    text = name,
                    fontWeight = FontWeight.Bold,
                    fontSize = 14.sp
                )
                // reply
                if (inReplyTo != null) {
                    Text(
                        text = "Reply: ${inReplyTo.removePrefix("https://")}",
                        fontSize = 12.sp,
                        overflow = TextOverflow.Ellipsis
                    )
                }
                // content
                Text(
                    text = content,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Medium,
                    softWrap = true
                )
                // published datetime
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = published,
                    fontSize = 13.sp,
                    fontWeight = FontWeight.Medium,
                    textAlign = TextAlign.Right
                )
            }
        }
    }
}

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun NoteAttachments(
    onClickImage: (String) -> Unit = {},
    attachment: List<MediaObject>
) {
    // attachments
    FlowRow(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        maxItemsInEachRow = 2,
    ) {
        val config = LocalConfiguration.current
        val cellWidth = (config.screenWidthDp.dp - 24.dp) / 2

        attachment.forEach {
            Box(
                modifier = Modifier
                    .width(cellWidth)
                    .wrapContentHeight()
                    .clickable { onClickImage(it.url) }
            ) {
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(it.url)
                        .crossfade(true)
                        .build(),
                    contentDescription = it.url
                )
            }
        }
    }
}

@Composable
private fun NoteActionsBarWithDateTime(
    published: String,
    onCreateReply: (() -> Unit)?,
    onAnnounce: (() -> Unit)?,
    isAnnounced: Boolean = false,
) {
    Row {
        Text(text = published)

        NoteActions(
            onCreateReply = onCreateReply,
            onAnnounce = onAnnounce,
            isAnnounced = isAnnounced
        )
    }
}

@OptIn(ExperimentalLayoutApi::class)
@Composable
private fun NoteActions(
    onCreateReply: (() -> Unit)?,
    onAnnounce: (() -> Unit)?,
    isAnnounced: Boolean = false,
    onOpenMenu: (() -> Unit)? = null,
) {
    // 初期状態のアイコンの色
    val defaultIconsColor = MaterialTheme.colors.onSurface
    // 触られた時のアイコンの色
    val activatedIconColor = ActivatedAnnounceColor

    val iconSize = 18.dp
    val horizontalPadding = 2.dp

    FlowRow(
        modifier = Modifier
            .fillMaxWidth()
            .height(horizontalPadding * 2 + iconSize) // horizontalなパディングとアイコンサイズ
            .padding(horizontal = horizontalPadding),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        Box {
            IconButton(onClick = { onCreateReply?.invoke() }) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Icon(
                        imageVector = Icons.Filled.Send,
                        contentDescription = "Reply",
                        tint = defaultIconsColor,
                        modifier = Modifier.size(iconSize)
                    )
                }
            }
        }

        Box {
            IconButton(onClick = { onAnnounce?.invoke() }) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    val foregroundColor = if (isAnnounced) { activatedIconColor } else { defaultIconsColor }
                    Icon(
                        imageVector = Icons.Filled.Notifications,
                        contentDescription = "Announce",
                        tint = foregroundColor,
                        modifier = Modifier.size(24.dp)
                    )
                }
            }
        }

        Box {
            IconButton(onClick = { onOpenMenu?.invoke() }) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Icon(
                        imageVector = Icons.Filled.Menu,
                        contentDescription = "Activity Actions Menu",
                        tint = defaultIconsColor,
                        modifier = Modifier.size(iconSize)
                    )
                }
            }
        }
    }
}