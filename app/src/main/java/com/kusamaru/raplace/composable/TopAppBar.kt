package com.kusamaru.raplace.composable

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable

@Composable
fun TopAppBarWithReturn(
    title: String,
    onClickReturn: () -> Unit
) {
    TopAppBar(
        title = { Text(text = title) },
        navigationIcon = {
            IconButton(onClick = onClickReturn) {
                Icon(Icons.Filled.ArrowBack, contentDescription = "Back to previous screen")
            }
        }
    )
}